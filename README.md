# toying with sockets

### What's going on here?

I was aked to toy a bit with sockets and retrieve data in a JSON format from a source every 30sec and then send it, after a bit of parsing and formatting, through several socket channels. Also, if I felt like it, I could put on some auth and put behind it a route to fetch the parsed data. I did all that and added some tests, lint and a small quality pipeline which prevent merging branches if either the test or lint routine fails. I also added a little badge displaying the code coverage in the repo because it looks nice. No one can push to the `main` branch. Finally, I was also asked to create a docker image as a bonus step.


### How do I play with it?

- Well, first, clone the project
- Type `yarn` at the root of the project to fetch all the dependencies
- Type `yarn run watch` to start the server. Any modification you make in the source code while running the server will be watched and trigger recompiling of the code and restart the server. Thanks nodemon :)
- You can now go on [http://localhost:3000](http://localhost:3000), there you'll see the formatted data being updated every 30sec. To witness it just open the console in your browser and check for the 'updating ...' logs which will be displayed... well, every 30sec. As the data comes from a static source and remains the same, it's the only way you can witness something happening


### I want to access the protected api route

For the protected route I used the 'express-openid-connect' lib which concentrates all the awesomeness of **auth0** for express. auth0 is a wonderful tool used to handle auth without having to setup your own custom logic with a database and all the trouble that comes with it. **BUT**, for it to work you need to create a `.env` file at the root of the project and fill it with some variables provided by auth0. I didn't commit the .env file because one should never do that, but in the context of this projet, for it to be testable without too much hassle, I will provide you with the variables needed to play with auth. Just copy/paste the following inside the .env file:

```
ISSUER_BASE_URL=https://dev-4hswoevs.eu.auth0.com
CLIENT_ID=EJvyZuPWuULAJ8qaf1c2GLVXBJf0EHHf
BASE_URL=http://localhost:3000
SECRET=de0beef8fd5f1a0042fd565a5ddf6e34c1b01b6caf98771e699b630fc4fe524b65393d9f36920037dd5d878776f70c0f0b2c856d08601b023e4a6f59aa2e5307
```

Once it's done, save the file and that's it, you're good to go! The `dotenv` lib will know where to get the needed variables. All you have to do now is go to [http://localhost:3000/api/match](http://localhost:3000/api/match) and you'll see a sweet auth page. Log in any way you want, either by creating a user with a password or just using the nice google connection button. Once logged in you'll see the formatted data. To log out, just go to [http://localhost:3000/logout](http://localhost:3000/logout).


### What about that docker image thing?

- To create the docker image just run the following command: `docker build --tag node-docker .`
- Make sure the image was created: `docker images`, you'll see your image listed as 'node-docker' in the REPOSITORY column
- Start the server as a docker image: `docker run -d --publish 3000:3000 node-docker`, let's break it down:   
**-d** --> is there to run the docker image in detached mode   
**--publish** --> the docker image runs the server in its own environment, including its own network. To be able to call the api you need to expose the 3000 port inside the container to your own local 3000 port, thus we give the local port we want to use first and then the port inside the container we want it to match to. Hence the 3000:3000. If you wanted to expose the 3000 port from the container to your 8000 port, you would type 8000:3000.  
**node-docker** --> is the name of your image
- You can now check the server is running: `docker ps`, it will be listed as node-docker in the IMAGE column
- Then, you can play with the project: go to [http://localhost:3000](http://localhost:3000) or [http://localhost:3000/api/match](http://localhost:3000/api/match) and you'll see the two urls are behaving the same way it would by launching the server with `yarn run watch`
- Finally, you can stop the container by typing `docker stop <NAME OF THE IMAGE>`, to get the name of your image, just use the `docker ps` command and you'll find it under the NAME column


Aaaaaand that's pretty much of it for this project!
 