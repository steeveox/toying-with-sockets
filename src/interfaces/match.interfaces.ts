export interface BaseMatch {
  time: string
  date: string
}

export interface ParsedMatch {
  kickoffTime: Date
  time?: never
  date?: never
}

export interface BaseHeatMaps {
  positions: string
}

export interface ParsedHeatMaps {
  positions?: never
}

interface GenericLineUps {
  [key: string]: any
}

interface GenericPlayerStats {
  [key: string]: any
}

export interface MatchData {
  match: BaseMatch
  heatmaps: BaseHeatMaps[]
  lineups: GenericLineUps
  playerstats: GenericPlayerStats
}

export interface ParsedMatchData {
  parsedMatch: ParsedMatch
  parsedHeatMaps: ParsedHeatMaps[]
  lineups: GenericLineUps
  playerstats: GenericPlayerStats
}
