import { MatchService } from "./match.service"
import {
  MatchData,
  ParsedMatchData,
  BaseMatch,
  ParsedMatch,
  BaseHeatMaps,
  ParsedHeatMaps,
} from "../interfaces/match.interfaces"

const service = new MatchService()
const payload = {
  match: {
    date: "2020-11-21Z",
    time: "20:00:00Z",
  },
  heatmaps: [
    {
      positions: "0,1,2,3,4,5,6",
    },
  ],
  lineups: [{}],
  playerstats: [{}],
}

describe("ParseMatchDatas", () => {
  it("Shoud strip time and date from match and add kickoffTime", () => {
    const test = service.ParseMatchDatas(payload)

    expect(test.parsedMatch.kickoffTime).toStrictEqual(
      new Date("2020-11-21T20:00:00.000Z")
    )
    expect(test.parsedMatch.time).not.toBeDefined()
    expect(test.parsedMatch.date).not.toBeDefined()
  })

  it("Shoud strip positions form heatmaps", () => {
    const test = service.ParseMatchDatas(payload)

    expect(test.parsedHeatMaps).toHaveLength(1)
    expect(test.parsedHeatMaps[0].positions).not.toBeDefined()
  })

  it("Shoud keep lineups and playerstats unchanged", () => {
    const test = service.ParseMatchDatas(payload)

    expect(test.lineups).toBe(payload.lineups)
    expect(test.playerstats).toBe(payload.playerstats)
  })
})
