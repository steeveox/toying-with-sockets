import {
  MatchData,
  ParsedMatchData,
  BaseMatch,
  ParsedMatch,
  BaseHeatMaps,
  ParsedHeatMaps,
} from "../interfaces/match.interfaces"

export class MatchService {
  public ParseMatchDatas(matchData: MatchData): ParsedMatchData {
    const parsedMatch = this.parseMatch(matchData.match)
    const parsedHeatMaps = this.parseHeatMaps(matchData.heatmaps)

    return {
      parsedMatch,
      parsedHeatMaps,
      playerstats: matchData.playerstats,
      lineups: matchData.lineups,
    }
  }

  protected parseMatch(match: BaseMatch): ParsedMatch {
    const kickoffTime = new Date(`${match.date}:${match.time}`)
    const { time, date, ...rest } = match
    return {
      ...rest,
      kickoffTime,
    }
  }

  protected parseHeatMaps(heatmaps: BaseHeatMaps[]): ParsedHeatMaps[] {
    const parsedHeatMaps: ParsedHeatMaps[] = []
    for (const heatmap of heatmaps) {
      const { positions, ...rest } = heatmap
      parsedHeatMaps.push(rest)
    }
    return parsedHeatMaps
  }
}
