/* eslint-disable */
import express from "express"
import * as path from "path"
import axios, { AxiosResponse } from "axios"
import { MatchService } from "./services/match.service"
import * as dotenv from "dotenv"

dotenv.config()

const matchService = new MatchService()

// fetch data and format it using the match service
const getData = async () => {
  const response: AxiosResponse = await axios.get(
    `https://storage.googleapis.com/app-resources-immersiv/TestTechnique/match.json`
  )

  return matchService.ParseMatchDatas({
    heatmaps: response.data.heatmaps,
    lineups: response.data.lineups,
    match: response.data.match,
    playerstats: response.data.playerStats,
  })
}

const app = express()
app.set("port", process.env.PORT || 3000)

const { auth, requiresAuth } = require('express-openid-connect');
// middleware used for auth
app.use(
  auth({
    authRequired: false,
    auth0Logout: true,
    issuerBaseURL: process.env.ISSUER_BASE_URL,
    baseURL: process.env.BASE_URL,
    clientID: process.env.CLIENT_ID,
    secret: process.env.SECRET,
  })
);

// little html page displaying parsed match results updated every 30secs via socket.io
app.get("/", (req: any, res: any) => {
  res.sendFile(path.resolve("./client/index.html"))
})

// display parsed data fetch once via api, requires auth
app.get("/api/match", requiresAuth(), async (req: any, res: any) => {
  res.send(await getData())
})


const http = require("http").Server(app)
const io = require("socket.io")(http)

// logic handling socket.io communication when a user connects
io.on("connection", (socket: any) => {
  (async function sendData() { 
    const { parsedMatch, parsedHeatMaps, playerstats, lineups } = await getData()
    
    socket.emit("match", JSON.stringify(parsedMatch))
    socket.emit("heatmaps", JSON.stringify(parsedHeatMaps))
    socket.emit("lineups", JSON.stringify(lineups))
    socket.emit("playerstats", JSON.stringify(playerstats))

    setTimeout(sendData, 30000)
  })()

})

const server = http.listen(3000, () => {
  console.log("listening on *:3000")
})
